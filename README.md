Commands
--------
Pre-required: docker installed, and docker daemon started

* start our custom jenkins master
```
docker-compose up
```
* extract plugin installed, to be able to rebuild image (from scratch)
```
./extract_plugin.sh > plugins.txt
```

Info
----
A custom image of jenkins is defined to allow:
* to define plugins

Try to use pipeline, and to run build,... into docker image (vs installing tools in docker master)

Setup Docker Daemon
-------------------

For security (see links) jenkins docker should communicate via https (and not via unix socket) + authorization plugin
* [Protect the Docker daemon socket | Docker Documentation](https://docs.docker.com/engine/security/https/)
* [Authobot](https://github.com/ndeloof/authobot)

```
#see https://docs.docker.com/engine/security/https/ for details
mkdir -p $HOME/.docker/mycerts
cd $HOME/.docker/mycerts
openssl genrsa -aes256 -out ca-key.pem 4096
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=$HOST" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:$HOST,IP:10.10.10.20,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf
```

to find the ip
```
ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1
```
```
cp -v ~/.docker/machine/certs/{ca,cert,key}.pem ~/.docker
```
```
{
  "debug": true,
  "tls": true,
  "tlscert": "/Users/davidb/.docker/machine/certs/cert.pem",
  "tlskey": "/Users/davidb/.docker/machine/certs/key.pem",
}
```
//  "hosts": ["tcp://127.94.0.2:2376"]

from ndeloof at https://github.com/jenkinsci/docker/issues/263
```quote
@ndevenish I've read with care before closing this issue :P
let's resume for better understanding

To run docker commands from a docker-ized jenkins master, we need to let this master invoke docker API

Docker-in-docker has some serious infrastructure and security impacts. Master would then run in provileged mode, which just mean "I don't care about security" - until you fine tuned some AppArmor profile, USE AT YOUR OWN RISK
Bind mount for docker.sock require the user to run jenkins to have special privileges. Using --gorup-add can be used for this purpose. The fact docker-compose v3 did remove this option is a docker issue (based on docker swarm mode restrictions IIUC), nothing we can do here. Anyway, access to docker.sock is a major security issue, as allowing bind mount is. So USE AT YOUR OWN RISK
Jenkins master container can be started with some docker API TLS keys and DOCKER_HOST environment variable set so it can access the docker API with legitimate credentials. Main benefits is jenkins is then an identified user for docker daemon authorization module, and one can plug an authz plugin (like mine) to prevent abuses, or rely on Docker EE client bundles with comparable API restrictions.
So, as a resume, nothing we can do inside this docker image to make this "simpler" without lowering the container security.
```
Links
-----

* [Using Docker with Pipeline](https://jenkins.io/doc/book/pipeline/docker/) Official doc
* [How To Build Docker Images Automatically With Jenkins Pipeline](https://blog.nimbleci.com/2016/08/31/how-to-build-docker-images-automatically-with-jenkins-pipeline/)

links about docker in docker => docker run sibling docker (USE AT YOUR OWN RISK)
* Vulnerability: https://www.lvh.io/posts/dont-expose-the-docker-socket-not-even-to-a-container.html
* previously solution [~jpetazzo/Using Docker-in-Docker for your CI or testing environment? Think twice.](http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/)
* https://docs.docker.com/engine/reference/commandline/dockerd/#miscellaneous-options
* https://docs.docker.com/engine/admin/#configure-the-docker-daemon
* https://docs.docker.com/engine/extend/plugins_authorization/#default-user-authorization-mechanism

Issues:
* [Use docker inside docker with jenkins user](https://github.com/jenkinsci/docker/issues/263)
* [Document how to connect to Docker host from container](https://github.com/moby/moby/issues/1143)
* [Using docker build plugin and getting "Cannot connect to the Docker daemon."](https://github.com/jenkinsci/docker/issues/196)

Todo
----

* ? setup jenkins lint plugin (see video/slide Jenkins_World_2017_-_The_Game_of_DevOps_-_Applying_Jenkins_Best_Practices_in_a_Dynamic_Industry-DVejh9AiQrY.mp4)
* naming convention
* job trigger subproject
