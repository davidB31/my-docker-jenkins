#!/bin/bash

# whoami
if [ -S ${DOCKER_SOCKET} ]; then
    DOCKER_GID=$(stat -c '%g' ${DOCKER_SOCKET})
    groupadd -for -g ${DOCKER_GID} docker
    # if the group already exist the gid was not set with groupadd (on alpine linux)
    groupmod -g ${DOCKER_GID} docker
    usermod -aG docker jenkins
    grep docker /etc/group
    ls -l ${DOCKER_SOCKET}
fi
#/bin/tini -- /usr/local/bin/jenkins.sh
exec sudo -E -H -u jenkins bash -c /usr/local/bin/jenkins.sh
